package course.live.email.sparkpost;

import com.sparkpost.Client;
import com.sparkpost.exception.SparkPostException;
import course.live.common.Mail;
import course.live.common.MailProvider;

public class SparkPostMailProvider implements MailProvider {


    public boolean send(Mail message) {
        String API_KEY = "YOUR API KEY HERE!!!";
        Client client = new Client(API_KEY);

        try {
            client.sendMessage(
                    message.getFrom(),
                    message.getTo(),
                    message.getSubject(),
                    message.getBody(),
                    null);
            System.out.println("SparkPost - The email was sent.");
            return true;
        } catch (SparkPostException exception) {
            System.err.println("SparkPost - The email was not sent. Error message: "  + exception.getMessage());
            return false;
        }
    }
}
